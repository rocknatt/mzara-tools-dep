<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crystal_model extends Base_Model {

	
	function __construct()
	{
		parent::__construct();
	}

	public function get_table_list($condition = array())
	{
		//Todo : sort by last opened
		//Todo : search user_shared

		$sql_user = '';
		$sql_user .= ' v_crystal_table.id IN ( SELECT crystal_table_user.crystal_table_id FROM crystal_table_user WHERE crystal_table_user.user_id=' . $this->user->identity->id . ' )';

		return $this->db->select('*')
						->from('v_crystal_table')
						->where(array('user_id' => $this->user->identity->id))
						->or_where(array($sql_user => null))
						->order_by('date_ajout', 'asc')
						->get()
						->result();
	}

	public function get_column_list($table_id)
	{
		//Todo : sort by user desire

		return $this->db->select('*')
						->from('crystal_table_column')
						->where(array('crystal_table_id' => $table_id))
						->order_by('date_ajout', 'asc')
						->get()
						->result();
	}

	public function get_note_list($table_column_id = null)
	{
		$condition = array();
		if ($table_column_id == null) {
			$condition['user_id'] = $this->user->identity->id;
		}
		else{
			$condition['crystal_table_column_id'] = $table_column_id;
		}

		return $this->db->select('*')
						->from('v_note')
						->where($condition)
						->order_by('date_ajout', 'desc')
						->get()
						->result();
	}

	public function get_note_page_list($note_id)
	{
		$condition = array('note_id' => $note_id);

		return $this->db->select('*')
						->from('note_page')
						->where($condition)
						->order_by('date_ajout', 'asc')
						->get()
						->result();
	}

	public function get_note_ligne_list($note_page_id)
	{
		$condition = array('note_page_id' => $note_page_id);

		return $this->db->select('*')
						->from('v_note_ligne')
						->where($condition)
						->order_by('date_ajout', 'asc')
						->get()
						->result();
	}

	public function table_update($data)
	{
		//default param
		$_data = array();

		if (isset($data['name'])) {
			$_data['name'] = $data['name'];
		}

		$this->base->update_entity('crystal_table', array('id' => $data['id']), $_data);

		//Todo log table_id here
	}

	public function table_websocket($model, $data)
	{
		$user_shared_list = explode(',', $model->user_shared_list);
		array_push($user_shared_list, $model->user_id);

		foreach ($user_shared_list as $user_id) {
			//ct_t: crystal_table_table
			$this->hub->push_message('ct_t', array('data' => $data, 'table_id' => $model->id), $user_id);
		}
	}

	public function column_update($data)
	{
		//default param
		$_data = array();

		if (isset($data['name'])) {
			$_data['name'] = $data['name'];
		}

		if (isset($data['crystal_table_id']) && $data['crystal_table_id'] != '') {
			$_data['crystal_table_id'] = $data['crystal_table_id'];
		}

		$this->base->update_entity('crystal_table_column', array('id' => $data['id']), $_data);

		//Todo log table_id here
	}

	public function table_column_websocket_notification($model, $data)
	{
		$user_shared_list = explode(',', $model->crystal_table_user_shared_list);
		array_push($user_shared_list, $model->crystal_table_user_id);

		foreach ($user_shared_list as $user_id) {
			//ct_c: crystal_table_column
			$this->hub->push_message('ct_c', array('data' => $data, 'table_id' => $model->crystal_table_id, 'column_id' => $model->id), $user_id);
		}
	}

	public function note_update($data)
	{
		//default param
		$_data = array();

		if (isset($data['name'])) {
			$_data['name'] = $data['name'];
		}

		if (isset($data['crystal_table_column_id']) && $data['crystal_table_column_id'] != '') {
			$_data['crystal_table_column_id'] = $data['crystal_table_column_id'];
		}

		if (isset($data['note_etat_id']) && $data['note_etat_id'] != '') {
			$_data['note_etat_id'] = $data['note_etat_id'];
		}

		$this->base->update_entity('note', array('id' => $data['id']), $_data);
	}

	public function note_websocket_notification($model, $data)
	{
		$user_shared_list = explode(',', $model->crystal_table_user_shared_list);
		array_push($user_shared_list, $model->crystal_table_user_id);

		foreach ($user_shared_list as $user_id) {
			//ct_n: crystal_table_note
			$this->hub->push_message('ct_n', array('data' => $data, 'note_id' => $model->id, 'column_id' => $model->crystal_table_column_id), $user_id);
		}
	}

	public function note_ligne_update($data)
	{
		//default param
		$_data = array();

		if (isset($data['contenu'])) {
			$_data['contenu'] = $data['contenu'];
		}

		if (isset($data['commentaire'])) {
			$_data['commentaire'] = $data['commentaire'];
		}

		if (isset($data['note_ligne_etat_id']) && $data['note_ligne_etat_id'] != '') {
			$_data['note_ligne_etat_id'] = $data['note_ligne_etat_id'];
			$_data['user_changed_state_id'] = $this->user->identity->id;
			$_data['date_changed_state'] = get_date_time_now('Y-m-d H:i:s');
		}

		$this->base->update_entity('note_ligne', array('id' => $data['id']), $_data);
	}

	public function note_ligne_websocket_notification($model, $data)
	{
		$user_shared_list = explode(',', $model->crystal_table_user_shared_list);
		array_push($user_shared_list, $model->crystal_table_user_id);

		if (isset($data['note_ligne_etat_id']) && $data['note_ligne_etat_id'] != '') {
			$data['note_ligne_etat_id'] = $model->note_ligne_etat_id;
			$data['user_changed_state_id'] = $model->user_changed_state_id;
			$data['user_changed_state_view_name'] = $model->user_changed_state_view_name;
			$data['date_changed_state'] = $model->date_changed_state;
		}

		foreach ($user_shared_list as $user_id) {
			//ct_nl: crystal_table_note_ligne
			$this->hub->push_message('ct_nl', array('data' => $data, 'note_ligne_id' => $model->id, 'note_page_id' => $model->note_page_id), $user_id);
		}
	}

	public function table_share($i='')
	{
		# code...
	}

	public function get_user($debut = 0, $nb = 5)
	{
		return $this->db->select('v_user.id, v_user.nom AS view_name')
						->from('v_user')
						->where(array('id!=' => $this->user->identity->id))
						->limit($nb, $debut)
						->get()
						->result();
	}

	public function get_session()
	{
		$model = $this->base->find_where('crystal_table_session', array('user_id' => $this->user->identity->id));
		if ($model == null) {
			$this->base->add_entity('crystal_table_session', array('user_id' => $this->user->identity->id, 'note_id' => '{}'));

			return $this->get_session();
		}

		return $model;
	}

	public function can_edit_column($model)
	{
		if ($model == null) {
			return false;
		}

		if ($model->crystal_table_user_shared_list != null) {
			$user_shared_list = explode(',', $model->crystal_table_user_shared_list);
			foreach ($user_shared_list as $user_id) {
				if ($user_id == $this->user->identity->id) {
					return true;
				}
			}
		}

		if ($model->crystal_table_user_id != null) {
			return $model->crystal_table_user_id == $this->user->identity->id;
		}

		return $model->user_id == $this->user->identity->id;
	}

	public function can_edit_note($model)
	{
		if ($model == null) {
			return false;
		}

		if ($model->crystal_table_user_shared_list != null) {
			$user_shared_list = explode(',', $model->crystal_table_user_shared_list);
			foreach ($user_shared_list as $user_id) {
				if ($user_id == $this->user->identity->id) {
					return true;
				}
			}
		}

		if ($model->crystal_table_user_id != null) {
			return $model->crystal_table_user_id == $this->user->identity->id;
		}

		return $model->user_id == $this->user->identity->id;
	}

	public function can_edit_note_ligne($model)
	{
		if ($model == null) {
			return false;
		}

		if ($model->crystal_table_user_shared_list != null) {
			$user_shared_list = explode(',', $model->crystal_table_user_shared_list);
			foreach ($user_shared_list as $user_id) {
				if ($user_id == $this->user->identity->id) {
					return true;
				}
			}
		}

		if ($model->crystal_table_user_id != null) {
			return $model->crystal_table_user_id == $this->user->identity->id;
		}

		return $model->user_id == $this->user->identity->id;
	}

	public function can_see_table($model)
	{
		if ($model == null) {
			return false;
		}

		$user_shared_list = explode(',', $model->user_shared_list);
		foreach ($user_shared_list as $user_id) {
			if ($user_id == $this->user->identity->id) {
				return true;
			}
		}

		return $model->user_id == $this->user->identity->id;
	}

	public function can_see_table_column($model)
	{
		if ($model == null) {
			return false;
		}

		if ($model->crystal_table_user_shared_list != null) {
			$user_shared_list = explode(',', $model->crystal_table_user_shared_list);
			foreach ($user_shared_list as $user_id) {
				if ($user_id == $this->user->identity->id) {
					return true;
				}
			}
		}

		if ($model->crystal_table_user_id != null) {
			return $model->crystal_table_user_id == $this->user->identity->id;
		}

		return $model->user_id == $this->user->identity->id;
	}

	public function is_editable($model)
	{
		if ($model == null) {
			return false;
		}

		//todo : user shared too
		$user_shared_list = explode(',', $model->user_shared_list);
		foreach ($user_shared_list as $user_id) {
			if ($user_id == $this->user->identity->id) {
				return true;
			}
		}

		return $model->user_id == $this->user->identity->id;
	}

}

/* End of file account_model.php */
/* Location: ./application/models/account_model.php */