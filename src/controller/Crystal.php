<?php
namespace Mzara\Controller;

class Crystal {

	private $CI;
	public $data = array();

	function __construct($CI, $url_path)
	{
		$this->CI = $CI;

		$this->data['user'] = $this->CI->user->identity;
		// $this->CI->load->model(array('crystal_model' => 'crystal'));

		//Todo : se souvenir de la dernière table que l'utilisateur a ouvert

		//Todo : gérer ici les routes à appeler
	}

	public function index()
	{
		return $this->CI->layout->view('crystal/index', $this->data);
	}

	public function read()
	{
		return $this->CI->load->view('admin/crystal/read', $this->data);
	}

	public function init()
	{
		//init the connexion to remote
		$response = new stdClass();
		$response->ok = true;
		$response->title = $this->lang->line('std_story');

		return $this->CI->layout->json($response);
	}

	public function table_list()
	{
		//get list of table's user order by last updated

		$data_list = $this->crystal->get_table_list();

		foreach ($data_list as $key => $value) {
			$data_list[$key]->is_editable = $this->crystal->is_editable($value);
		}

		$response = new stdClass();
		$response->ok = true;
		$response->data_list = $data_list;

		return $this->CI->layout->json($response);
	}

	/**
	 * Create, update, delete crystal table. Just need to match sent object and it do the rest
	 * 
	 * @return json
	 */
	public function table_update()
	{
		$data_list = $this->input->post('data');
		$data_list = json_decode($data_list, true);

		foreach ($data_list as $data) {
			//search table
			$model = $this->base->find_where('crystal_table', array('id' => $data['id']));
			if ($model == null) {
				//create table
				$this->base->add_entity('crystal_table', array(
					'id' => $data['id'],
					'name' => $data['name'],
					'date_ajout' => get_date_time_now('Y-m-d H:i:s'),
					'user_id' => $this->user->identity->id,
					'crystal_table_etat_id' => $data['crystal_table_etat_id'],
				));
			}
			else if($this->crystal->is_editable($model)){

				if (isset($data['is_deleted'])) {
					$this->base->delete_entity_where('crystal_table', array('id' => $data['id']));

					$this->crystal->table_websocket($model, $data);
				}
				else{
					$this->crystal->table_update($data);
				}

			}

			//Todo : ws message
		}

		$response = new stdClass();
		$response->ok = true;

		$this->CI->layout->ob_start_json($response);

		//ws
		foreach ($data_list as $data) {
			//search table
			$model = $this->base->find_where('crystal_table', array('id' => $data['id']));

			if ($model != null) {
				$this->crystal->table_websocket($model, $data);
			}
		}

		//check if user can edit

		//return json ok
	}

	public function table_share($id)
	{
		$model = $this->base->find_where('crystal_table', array('id' => $id));

		if ($model == null) {
			return $this->CI->layout->error_404($this->data);
		}

		if (!$this->crystal->is_editable($model)) {
			return $this->CI->layout->error_403($this->data);
		}

		// share it
		$user_list = $this->input->post('user_list');
		$user_list = explode(',', $user_list);

		$chat_group_user_list = $this->base->get_list('crystal_table_user', array('crystal_table_id' => $model->id), 0, 1000, 'crystal_table_id', 'asc');
		$user_list_do_delete = array();
		foreach ($chat_group_user_list as $value) {
			if (!in_array($value->user_id, $user_list)) {
				array_push($user_list_do_delete, $value->user_id);
			}
		}

		//Effacer les utilisateus qui ne sont plus présents
		foreach ($user_list_do_delete as $user_id) {
			$this->base->delete_entity_where('crystal_table_user', array('crystal_table_id' => $model->id, 'user_id' => $user_id));
		}

		foreach ($user_list as $user_id) {

			$model = $this->base->find_where('crystal_table_user', array('user_id' => $user_id, 'crystal_table_id' => $id));
			if ($model == null) {
				$this->base->add_entity('crystal_table_user', array(
					'crystal_table_id' => $id,
					'user_id' => $user_id,
					'user_shared_id' => $this->user->identity->id,
					'date_ajout' => get_date_time_now('Y-m-d H:i:s'),
				));
			}			
		}

		$response = new stdClass();
		$response->ok = true;

		$this->CI->layout->ob_start_json($response);

		// notify user
		$this->CI->load->model('notification_model', 'notification');
		$not_link = 'crystal/index?table_id='. $model->id;

		foreach ($user_list as $user_id) {

			$not_message = array();
			$not_message['user_id_'. $this->user->identity->id] = $this->user->identity->id;
			$not_message['lang_2'] = 'std_shared';
			$not_message['lang_3'] = 'std_crystal_table';
			$not_message['lang_4'] = 'std_with_you';

			$this->notification->create(
				'std_crystal_table',
				json_encode($not_message),
				$not_link,
				'crystal_table',
				$id,
				$user_id
			);		
		}

		// send websocket message for sync { table_id, table_name }
	}

	public function table_column_update()
	{
		$data_list = $this->input->post('data');
		$data_list = json_decode($data_list, true);

		foreach ($data_list as $data) {
			//search table
			$model = $this->base->find_where('v_crystal_table_column', array('id' => $data['id']));
			if ($model == null) {
				//create table
				$this->base->add_entity('crystal_table_column', array(
					'id' => $data['id'],
					'name' => $data['name'],
					'date_ajout' => get_date_time_now('Y-m-d H:i:s'),
					'user_id' => $this->user->identity->id,
					'crystal_table_id' => $data['crystal_table_id'],
				));
			}
			else if($this->crystal->can_edit_column($model)){

				if (isset($data['is_deleted'])) {
					$this->base->delete_entity_where('crystal_table_column', array('id' => $data['id']));

					$this->crystal->table_column_websocket_notification($model, $data);
				}
				else{
					$this->crystal->column_update($data);
				}

			}

			//Todo : ws message
		}

		$response = new stdClass();
		$response->ok = true;

		$this->CI->layout->ob_start_json($response);

		//ws
		foreach ($data_list as $data) {
			//search table
			$model = $this->base->find_where('v_crystal_table_column', array('id' => $data['id']));
			

			if ($model != null) {
				$this->crystal->table_column_websocket_notification($model, $data);
			}
		}

		//search column

		//check if is editable

		//return json ok

		//send websocket message
	}

	public function table_column_list($table_id)
	{
		//search table
		$model = $this->base->find_where('v_crystal_table', array('id' => $table_id));

		if ($model == null) {
			return $this->CI->layout->error_404($this->data);
		}

		//can see table content
		if (!$this->crystal->can_see_table($model)) {
			return $this->CI->layout->error_403($this->data);
		}

		//get all table column list
		$data_list = $this->crystal->get_column_list($table_id);
		foreach ($data_list as $key => $value) {
			$data_list[$key]->is_editable = $this->crystal->is_editable($model);
		}

		$response = new stdClass();
		$response->ok = true;
		$response->data_list = $data_list;

		return $this->CI->layout->json($response);
	}

	public function note_update()
	{
		$data_list = $this->input->post('data');
		$data_list = json_decode($data_list, true);

		foreach ($data_list as $data) {
			//search table
			$model = $this->base->find_where('v_note', array('id' => $data['id']));
			if ($model == null) {
				//create table
				$this->base->add_entity('note', array(
					'id' => $data['id'],
					'name' => $data['name'],
					'date_ajout' => get_date_time_now('Y-m-d H:i:s'),
					'user_id' => $this->user->identity->id,
					'crystal_table_column_id' => $data['crystal_table_column_id'],
					'note_etat_id' => 1
				));

				if (isset($data['note_page'])) {
					$this->base->add_entity('note_page', array(
						'id' => $data['note_page']['id'],
						'page' => $data['note_page']['page'],
						'date_ajout' => get_date_time_now('Y-m-d H:i:s'),
						'user_id' => $this->user->identity->id,
						'note_id' => $data['id']
					));
				}
			}
			else if($this->crystal->can_edit_note($model)){

				if (isset($data['is_deleted'])) {
					$this->base->delete_entity_where('note', array('id' => $data['id']));

					$this->crystal->note_websocket_notification($model, $data);
				}
				else{
					$this->crystal->note_update($data);
				}

			}

			//Todo : ws message
		}

		$response = new stdClass();
		$response->ok = true;

		$this->CI->layout->ob_start_json($response);

		//ws
		foreach ($data_list as $data) {
			//search table
			$model = $this->base->find_where('v_note', array('id' => $data['id']));

			if ($model != null) {
				$this->crystal->note_websocket_notification($model, $data);
			}
		}
	}

	public function note_list($table_column_id = null)
	{
		$model = $this->base->find_where('v_crystal_table_column', array('id' => $table_column_id));

		if ($model == null) {
			return $this->CI->layout->error_404($this->data);
		}

		//can see table content
		if (!$this->crystal->can_see_table_column($model)) {
			return $this->CI->layout->error_403($this->data);
		}

		$data_list = $this->crystal->get_note_list($table_column_id);

		foreach ($data_list as $key => $value) {
			$data_list[$key]->is_editable = $this->crystal->can_edit_note($value);
		}

		$response = new stdClass();
		$response->ok = true;
		$response->data_list = $data_list;

		return $this->CI->layout->json($response);

		// if table_column is not null, search column

		// check if column is editable

		// get note_list

		//Log note
	}

	public function note_page_update()
	{
		$data_list = $this->input->post('data');
		$data_list = json_decode($data_list, true);

		foreach ($data_list as $data) {
			//search table
			$model = $this->base->find_where('note_page', array('id' => $data['id']));
			if ($model == null) {
				//create table
				$this->base->add_entity('note_page', array(
					'id' => $data['id'],
					'page' => $data['page'],
					'date_ajout' => get_date_time_now('Y-m-d H:i:s'),
					'user_id' => $this->user->identity->id,
					'note_id' => $data['note_id']
				));
			}
			//muré pour le moment
			// else if($this->crystal->can_edit_column($model)){

			// 	if (isset($data['is_deleted'])) {
			// 		$this->base->delete_entity_where('note', array('id' => $data['id']));
			// 	}
			// 	else{
			// 		$this->crystal->column_update($data);
			// 	}

			// }

			//Todo : ws message
		}

		$response = new stdClass();
		$response->ok = true;

		$this->CI->layout->ob_start_json($response);

		//search note

		//check if note is editable

		// send ws message
	}

	public function note_page_list($note_id)
	{
		$model = $this->base->find_where('note', array('id' => $note_id));

		if ($model == null) {
			return $this->CI->layout->error_404($this->data);
		}

		//can see table content
		// if (!$this->crystal->can_see_table_column($model)) {
		// 	return $this->CI->layout->error_403($this->data);
		// }

		$data_list = $this->crystal->get_note_page_list($note_id);

		$response = new stdClass();
		$response->ok = true;
		$response->data_list = $data_list;

		return $this->CI->layout->json($response);

		// if table_column is not null, search column

		// check if column is editable

		// get note_list

		//Log note
	}

	public function note_ligne_create($note_id, $note_page_id)
	{
		//searhc note

		//check if note is editable


		// return json ok

		//send ws message

	}

	public function note_ligne_update()
	{
		$data_list = $this->input->post('data');
		$data_list = json_decode($data_list, true);

		foreach ($data_list as $data) {
			//search table
			$model = $this->base->find_where('v_note_ligne', array('id' => $data['id']));

			if ($model == null) {
				//create table
				$this->base->add_entity('note_ligne', array(
					'id' => $data['id'],
					'contenu' => $data['contenu'],
					'note_page_id' => $data['note_page_id'],
					'date_ajout' => get_date_time_now('Y-m-d H:i:s'),
					'user_id' => $this->user->identity->id,
					'note_ligne_etat_id' => $data['note_ligne_etat_id']
				));
			}
			else if($this->crystal->can_edit_note_ligne($model)){

				if (isset($data['is_deleted'])) {
					$this->base->delete_entity_where('note_ligne', array('id' => $data['id']));

					$this->crystal->note_ligne_websocket_notification($model, $data);
				}
				else{
					$this->crystal->note_ligne_update($data);
				}

			}

			//Todo : ws message
		}

		$response = new stdClass();
		$response->ok = true;

		$this->CI->layout->ob_start_json($response);

		//ws
		foreach ($data_list as $data) {
			//search table
			$model = $this->base->find_where('v_note_ligne', array('id' => $data['id']));

			if ($model != null) {
				unset($data['get_focus']);
				$this->crystal->note_ligne_websocket_notification($model, $data);
			}
		}

		// search note_ligne

		//check if is editable

		//return json_ok

		//send ws message
	}

	public function note_ligne_poke($note_ligne_id, $user_id)
	{
		$model = $this->base->find_where('v_note_ligne', array('id' => $note_ligne_id));

		if ($model == null) {
			return $this->CI->layout->error_404($this->data);
		}

		$response = new stdClass();
		$response->ok = true;

		// $this->CI->layout->ob_start_json($response);

		// notify user
		$this->CI->load->model('notification_model', 'notification');
		$not_link = 'crystal/index?note_ligne_id='. $model->id;

		$not_message = array();
		$not_message['user_id_'. $this->user->identity->id] = $this->user->identity->id;
		$not_message['lang_2'] = 'std_warned_you_about_a_task';
		$not_message['text_1+/'] = ' : "' .  $model->contenu . '".';

		$this->notification->create(
			'std_crystal_table_remind',
			json_encode($not_message),
			$not_link,
			'crystal_table',
			$model->id,
			$user_id
		);

	}

	public function note_ligne_list($note_page_id)
	{
		$model = $this->base->find_where('note_page', array('id' => $note_page_id));

		if ($model == null) {
			return $this->CI->layout->error_404($this->data);
		}

		//can see table content
		// if (!$this->crystal->can_see_table_column($model)) {
		// 	return $this->CI->layout->error_403($this->data);
		// }

		$data_list = $this->crystal->get_note_ligne_list($note_page_id);

		foreach ($data_list as $key => $value) {
			$data_list[$key]->is_editable = $this->crystal->can_edit_note_ligne($value);
		}

		$response = new stdClass();
		$response->ok = true;
		$response->data_list = $data_list;

		return $this->CI->layout->json($response);

		// search page

		// check if user can see

		// return json ok
	}

	public function set_session()
	{
		$data = $this->input->post('data');
		$data = json_decode($data, true);

		$model = $this->crystal->get_session();

		$_data = array();
		$_data['crystal_table_id'] = $data['crystal_table_id'];
		$_data['note_id'] = json_encode($data['note_id']);

		$this->base->update_entity('crystal_table_session', array('user_id' => $this->user->identity->id), $_data);

		$response = new stdClass();
		$response->ok = true;

		return $this->CI->layout->json($response);
	}

	public function get_session()
	{
		$model = $this->crystal->get_session();
		$model->note_id = json_decode($model->note_id);

		$response = new stdClass();
		$response->ok = true;
		$response->data = $model;

		return $this->CI->layout->json($response);
	}

	public function get_user($debut = 0, $nb = 1000)
	{
		$key = $this->input->get('key');

		$user_list = $this->crystal->get_user($debut, $nb);

		//Recherche avec key
		if ($key != '') {
			$result = array();

			foreach ($user_list as $user) {
				if (strstr($user->view_name, $key)) {
					array_push($result, $user);
				}
			}

			$user_list = $result;
		}

		$response = new stdClass();
		$response->ok = true;
		$response->data_list = $user_list;

		return $this->CI->layout->json($response);
	}


}
